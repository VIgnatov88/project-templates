﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Models
{
    public class Book
    {
        public int LoanID { get; set; }
        public virtual Loan Loan { get; set; }
        public int ReservationID { get; set; }
        public virtual List<Reservation> Reservations { get; set; }
        public int ID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Subject { get; set; }
        public int Pages { get; set; }
        public int Year { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string Link { get; set; }
        public int Copys { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
