﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Library.Models
{
    public class Loan
    {
        [Key]
        public int ID { get; set; }

        public string UserID { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Book")]
        public int BookID { get; set; }
        public virtual Book Book { get; set; }
        public DateTime BookTakenAt { get; set; }
        public DateTime DateToBeReturned { get; set; }
    }
}
