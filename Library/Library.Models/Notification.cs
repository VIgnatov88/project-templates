﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Library.Models
{
    public class Notification
    {
        [Key]
        public int ID { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public DateTime DateSent { get; set; }

        public string Text { get; set; }

        public bool IsSeen { get; set; }
    }
}
