﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Models
{
    public class Reservation
    {
        public Reservation()
        {
        }
        public int ID { get; set; }

        public int BookID { get; set; }
        public virtual Book ReservedBook { get; set; }

        public string UserID { get; set; }
        public virtual User Reserver { get; set; }
    }
}
