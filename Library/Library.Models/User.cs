﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Library.Models
{
    public class User : IdentityUser    
    {
        public bool IsBanned { get; set; }
        public DateTime MembershipStart { get; set; }
        public bool PaidFee()
        {
            if (DateTime.Now > MembershipStart + TimeSpan.FromDays(30))
            {
                return false;
            }
            return true;
        }
        public virtual ICollection<Loan> Loans { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
    }
}
