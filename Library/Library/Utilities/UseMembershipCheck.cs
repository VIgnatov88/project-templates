﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Utilities
{
    public static class MembershipCheck
    {
        public static IApplicationBuilder UseMemebershipCheck(this IApplicationBuilder app)
        {
            return app.UseMiddleware<MembershipCheckMiddleware>();
        }
    }
}
