﻿using Library.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Library.Utilities
{
    public class MembershipCheckMiddleware
    {
        private readonly RequestDelegate _next;

        public MembershipCheckMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext httpContext, UserManager<User> userManager)
        {
            if (httpContext.User == null)
            {
                await _next(httpContext);
            }

            var user = await userManager.GetUserAsync(httpContext.User);
            if (user != null)
            {
                if (!user.PaidFee() && httpContext.Request.Path != "/User/PayMembership")
                {
                    httpContext.Response.Redirect("/User/PayMembership", false);
                }
            }
            await _next(httpContext);
        }
    }
}
