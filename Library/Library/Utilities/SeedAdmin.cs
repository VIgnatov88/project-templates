﻿using Library.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Seed
{
    public static class SeedAdmin
    {
        public static async Task Plant(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var userManager = scope.ServiceProvider.GetService<UserManager<User>>();
                var roleManager = scope.ServiceProvider.GetService<RoleManager<IdentityRole>>();

                if (!await roleManager.RoleExistsAsync("Librarian"))
                {
                    var role = new IdentityRole("Librarian");
                    await roleManager.CreateAsync(role);
                    var librarian = new User
                    {
                        Email = "Vasil_0016@abv.bg",
                        UserName = "VaskoBatal"
                    };
                    await userManager.CreateAsync(librarian, "konche123");
                    await userManager.AddToRoleAsync(librarian, "Librarian");
                }
            }
        }
    }
}
