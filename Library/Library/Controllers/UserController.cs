﻿using Library.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Library.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<User> userManager;

        public UserController(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        public IActionResult PayMembership()
        {
            return View();
        }

        public async Task<IActionResult> Pay()
        {
            var user = await userManager.GetUserAsync(this.User);
            user.MembershipStart = DateTime.Now;
            await userManager.UpdateAsync(user);

            return Redirect("/Home/Index");
        }
    }
}