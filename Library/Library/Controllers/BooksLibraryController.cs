﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Mappers;
using Library.Models;
using Microsoft.AspNetCore.Mvc;
using Library.Service.Contracts;
using Microsoft.AspNetCore.Identity;

namespace LibrarySystemWeb.Controllers
{
    public class BooksLibraryController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IBookServices bookServices;
        private readonly IReservationServices reservationServices;
        public BooksLibraryController(IBookServices bookServices, UserManager<User> userManager,
            IReservationServices reservationServices)
        {
            this.bookServices = bookServices;
            this.userManager = userManager;
            this.reservationServices = reservationServices;
        }
        public async Task<IActionResult> Library()
        {
            var books = await bookServices.GetCollectionAsync();
            var vm = new HomeViewModel();
            vm.Books = books.Select(book => book.MapToViewModel()).ToList();
            return View(vm);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new BookViewModel());
        }
        [HttpPost]
        public async Task<IActionResult> Create(BookViewModel model)
        {

            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var book = await bookServices.CreateBook(model.Title, model.Author, model.Subject, model.Pages, model.Year, model.Country, model.Language, model.Link, model.Copys);
                await this.bookServices.AddBookCopysAsync(book, book.Copys);
                return RedirectToAction("Library", "BooksLibrary");
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return BadRequest();
            }
        }
        [HttpGet]
        public async Task<IActionResult> CreateBookCopy(int id)
        {
            try
            {
                var searchedBook = await this.bookServices.GetAsync(id);
                searchedBook.Copys++;
                var bookCopy = await this.bookServices.CreateBook(searchedBook.Title, searchedBook.Author, searchedBook.Subject, searchedBook.Pages, searchedBook.Year, searchedBook.Country, searchedBook.Language, searchedBook.Link, searchedBook.Copys);
                bookCopy.Copys = searchedBook.Copys;

                return RedirectToAction("Library", "BooksLibrary");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Create", "BooksLibrary");
            }
        }

        //[HttpGet]
        //public async Task<IActionResult> Search([FromQuery]SearchViewModel viewModel)
        //{
        //    if (string.IsNullOrWhiteSpace(viewModel.Title) ||
        //        viewModel.Title.Length < 3)
        //    {
        //        return View();
        //    }

        //    viewModel.Searchresults = (await this.bookServices
        //        .SearchBookByTitleAsync(viewModel.Title))
        //        .Select(book => book.MapToViewModel())
        //        .ToList();
        //    return View(viewModel);
        //}

        [HttpGet]
        public async Task<IActionResult> Search(string title)
        {
            if (string.IsNullOrWhiteSpace(title) ||
                title.Length < 3)
            {
                return PartialView("_SearchResultsPartial", new SearchViewModel());
            }
            var model = new SearchViewModel
            {
                Title = title,
                Searchresults = new List<BookViewModel>
                {
                    new BookViewModel { Title = title },
                    new BookViewModel { Title = title.ToUpper() },
                    new BookViewModel { Title = title.ToLower() },
                }
            };
            return PartialView("_SearchResultsPartial", model);
        }

[HttpGet]
public async Task<IActionResult> DeleteBook(int id)
{
    try
    {
        var searchedBook = await this.bookServices.GetAsync(id);
        searchedBook.Copys--;
        var copies = (await this.bookServices
            .GetCollectionAsync())
            .Where(b => b.Title == searchedBook.Title && b.Author == searchedBook.Author);
        foreach (var copy in copies)
        {
            copy.Copys--;
        }

        await this.bookServices.DeleteAsync(searchedBook);

        return RedirectToAction("Library", "BooksLibrary");
    }
    catch (Exception ex)
    {
        return RedirectToAction("Delete", "BooksLibrary");
    }
}
[HttpGet]
public IActionResult Delete()
{
    return View();
}
public async Task<IActionResult> ReserveBook(int id)
{
    var book = await this.bookServices.GetAsync(id);
    var user = await this.userManager.GetUserAsync(HttpContext.User);
    var reservation = await this.reservationServices.CreateReservation(user, book);
    return RedirectToAction("Library", "BooksLibrary");
}
public async Task<IActionResult> ManageBooks([FromQuery]SearchViewModel viewModel)
{
    return View(new SearchViewModel());
}
    }
}