﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class BookViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter a title.")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Please enter an author.")]
        public string Author { get; set; }
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid page number.")]
        public int Pages { get; set; }
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter a valid number of copys.")]
        public int Copys { get; set; }
        [Required]
        public string Language { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public int Year { get; set; }
        public string Link { get; set; }
        public string Country { get; set; }
    }
}
