﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class HomeViewModel
    {
        public List<BookViewModel> Books { get; set; }
    }
}
