﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class UserViewModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public IReadOnlyCollection<BookViewModel> ReservedBooks { get; set; }
    }
}
