﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            Searchresults = new List<BookViewModel>();
        }
        [Required]
        [MinLength(3, ErrorMessage = "Please provide at least 3 letters")]  
        public string Title { get; set; }
        public int Id { get; set; }
        public IReadOnlyCollection<BookViewModel> Searchresults { get; set; } = new List<BookViewModel>();
    }
}
