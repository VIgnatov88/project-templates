﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Mappers
{
    public static class MapperExtensions
    {
        public static BookViewModel MapToViewModel(this Book book)
        {
            var viewModel = new BookViewModel();
            viewModel.Id = book.ID;
            viewModel.Author = book.Author;
            viewModel.Title = book.Title;
            viewModel.Pages = book.Pages;
            viewModel.Language = book.Language;
            viewModel.Copys = book.Copys;
            viewModel.Link = book.Link;
            viewModel.Subject = book.Subject;
            viewModel.Year = book.Year;
            viewModel.Country = book.Country;
            return viewModel;
        }
        public static UserViewModel MapToViewmodel(this User user)
        {
            var viewModel = new UserViewModel();
            viewModel.UserId = user.Id;
            viewModel.Name = user.NormalizedUserName;
            return viewModel;
        }
    }
}
