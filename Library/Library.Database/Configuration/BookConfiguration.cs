﻿using Library.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Library.Database1.Configuration
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasKey(b => b.ID);

            builder
                .HasOne(x => x.Loan)
                .WithOne(b => b.Book);
                //.HasForeignKey();
            builder
                .HasMany(b => b.Reservations)
                .WithOne(x => x.ReservedBook)
                .HasForeignKey(x => x.BookID);

            builder.Property(b => b.Author)
                 .IsRequired().HasMaxLength(100);

            builder.Property(b => b.Title)
                .IsRequired().HasMaxLength(100);

            builder.Property(b => b.Subject)
                .IsRequired().HasMaxLength(100);

            builder.Property(b => b.Pages)
                .IsRequired().HasMaxLength(100);

            builder.Property(b => b.Year)
                .IsRequired().HasMaxLength(100);

            builder.Property(b => b.Country)
                .IsRequired().HasMaxLength(100);

            builder.Property(b => b.Language)
                .IsRequired().HasMaxLength(100);

            builder.Property(b => b.Link)
                .HasMaxLength(100);

            builder.Property(b => b.Copys)
                .IsRequired();

            builder.Property(b => b.CreatedOn)
                .IsRequired();

        }
    }
}
