﻿using Library.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Database1.Configuration
{
    public class NotificationConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder
                .Property(a => a.DateSent)
                .IsRequired();
            builder.Property(a => a.Text)
                .IsRequired();
            builder.Property(a => a.IsSeen)
                .IsRequired();
        }
    }
}
