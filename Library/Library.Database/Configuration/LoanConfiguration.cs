﻿using Library.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Database1.Configuration
{
    public class LoanConfiguration : IEntityTypeConfiguration<Loan>
    {
        public void Configure(EntityTypeBuilder<Loan> builder)
        {
            builder
                .HasKey(l => l.ID);
            builder.Property(l => l.BookTakenAt)
                .IsRequired();
            builder.Property(l => l.BookID)
                .IsRequired();
            builder.Property(l => l.UserID)
                .IsRequired();
            builder.Property(l => l.BookTakenAt)
                .IsRequired();
            builder.Property(l => l.DateToBeReturned)
                .IsRequired();
        }
    }
}
