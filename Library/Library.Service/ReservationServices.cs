﻿using Library.Database1;
using Library.Models;
using Library.Service.Contracts;
using Library.Service.Utilities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Service
{
    public class ReservationsServices : IReservationServices
    {
        private readonly LibraryDbContext context;

        public ReservationsServices(LibraryDbContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(Reservation reservation)
        {
            reservation.EnsureNotNull();
            await context.Reservations.AddAsync(reservation);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Reservation reservations)
        {
            reservations.EnsureNotNull();
            context.Reservations.Remove(reservations);
            await context.SaveChangesAsync();
        }

        public async Task<Reservation> GetAsync(int id)
        {
            var reservation = await context.Reservations.FirstOrDefaultAsync(r => r.ID == id);
            reservation.EnsureNotNull();
            return reservation;
        }

        public async Task<IReadOnlyCollection<Reservation>> GetCollectionAsync()
        {
            return await context.Reservations.ToListAsync();
        }

        public async Task<User> GetReserverAsync(Book book)
        {
            var reservation = await context.Reservations.FirstOrDefaultAsync(r => r.ReservedBook == book);
            if (reservation == null)
                return null;
            return reservation.Reserver;
        }
        public async Task<Reservation> CreateReservation(User account, Book book)
        {
            var reservation = new Reservation
            {
                Reserver = account,
                UserID = account.Id,
                ReservedBook = book,
                BookID = book.ID,
            };

            reservation.EnsureNotNull();
            await this.context.Reservations.AddAsync(reservation);
            return reservation;
        }
    }
}
