﻿using Library.Database1;
using Library.Models;
using Library.Service.Contracts;
using Library.Service.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Service
{
    public class BookServices : IBookServices   
    {
        private readonly LibraryDbContext context;

        public BookServices(LibraryDbContext context)
        {
            this.context = context;
        }
        public async Task AddAsync(Book book)
        {
            book.EnsureNotNull();
            await context.Books.AddAsync(book);
            await context.SaveChangesAsync();
        }
        public async Task DeleteAsync(Book book)
        {
            book.EnsureNotNull();
            context.Books.Remove(book);
            await context.SaveChangesAsync();
        }
        public async Task<Book> GetAsync(int id)
        {
            var book = await context.Books.FirstOrDefaultAsync(a => a.ID == id);
            book.EnsureNotNull();
            return book;
        }
        public async Task UpdateByTitleAsync(string BookTitle, string newTitle)
        {
            var searchedBook = await FindBookByTitleAsync(BookTitle);
            searchedBook.EnsureNotNull();
            searchedBook.Title = newTitle;
            await context.SaveChangesAsync();
        }
        public async Task<Book> FindBookByTitleAsync(string title)
        {
            var book = await context.Books.FirstOrDefaultAsync(a => a.Title == title);
            book.EnsureNotNull();
            return book;
        }
        public async Task<IReadOnlyCollection<Book>> GetCollectionAsync()
        {
            return await context.Books.ToListAsync();
        }
        public async Task UpdateByBookAsync(Book book, string newTitle)
        {
            book.Title = newTitle;
            await context.SaveChangesAsync();
        }
        public async Task<IReadOnlyCollection<Book>> SearchBookByTitleAsync(string title)
        {
            var books = await context.Books.Where(b => b.Title.Contains(title)).ToListAsync();
            return books;
        }

        public async Task<Book> CreateBook(string title, string author, string subject, int pages, int year, string country, string language, string link, int copys)
        {
            var book = new Book
            {
                Title = title,
                Author = author,
                Subject = subject,
                Pages = pages,
                Year = year,
                Country = country,
                Language = language,
                Link = link,
                Copys = copys,
                CreatedOn = DateTime.Now
            };
            book.EnsureNotNull();
            await this.context.Books.AddAsync(book);
            await context.SaveChangesAsync();
            return book;
        }
        public async Task AddBookCopysAsync(Book book, int timesToBeAdded)
        {
            if (timesToBeAdded < 0)
             throw new ArgumentException(); 
            book.EnsureNotNull();
            for (int i = 0; i < timesToBeAdded; i++)
            {
                var bookCopy = CreateBook(book.Title, book.Author, book.Subject, book.Pages, book.Year, book.Country, book.Language, book.Link, book.Copys); 
            }
        }
    }
}