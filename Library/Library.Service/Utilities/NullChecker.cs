﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Service.Utilities
{
    public static class NullChecker
    {
        public static void EnsureNotNull(this object o)
        {
            if (o == null)
            {
                throw new ArgumentNullException();
            }
        }
    }
}
