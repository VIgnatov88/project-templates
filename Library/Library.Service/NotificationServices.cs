﻿using Library.Database1;
using Library.Models;
using Library.Service.Contracts;
using Library.Service.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Service
{
    public class NotificationServices : INotificationServices
    {
        private readonly LibraryDbContext context;

        public NotificationServices(LibraryDbContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(Notification notification)
        {
            notification.EnsureNotNull();
            await context.Notifications.AddAsync(notification);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Notification notification)
        {
            notification.EnsureNotNull();
            context.Notifications.Remove(notification);
            await context.SaveChangesAsync();
        }

        public async Task<Notification> GetAsync(int id)
        {
            var notification = await context.Notifications.FirstOrDefaultAsync(n => n.ID == id);
            notification.EnsureNotNull();

            return notification;
        }

        public async Task UpdateTextAsync(Notification notification, string newText)
        {
            notification.EnsureNotNull();
            notification.Text = newText;
            await context.SaveChangesAsync();
        }
    }
}
