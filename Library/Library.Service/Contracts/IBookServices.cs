﻿using Library.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Service.Contracts
{
    public interface IBookServices : IEntityServices<Book>
    {
        Task<IReadOnlyCollection<Book>> GetCollectionAsync();
        Task UpdateByTitleAsync(string title, string newTitle);
        Task UpdateByBookAsync(Book book, string newTitle);
        Task<Book> FindBookByTitleAsync(string title);
        Task<IReadOnlyCollection<Book>> SearchBookByTitleAsync(string title);
        Task<Book> CreateBook(string title, string author, string subject, int pages, int year, string country, string language, string link,int copys);
        Task AddBookCopysAsync(Book book, int timesToBeAdded);
    }
}
