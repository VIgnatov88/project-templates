﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.Service.Contracts
{
    public interface IEntityServices<T>
    {
        Task AddAsync(T book);
        Task DeleteAsync(T book);
        Task<T> GetAsync(int id);
    }
}
