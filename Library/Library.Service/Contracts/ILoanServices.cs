﻿using Library.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Service.Contracts
{
    public interface ILoanServices : IEntityServices<Loan>
    {
        Task<User> GetAccountRenterAsync(Book book);
        Task<IReadOnlyCollection<Loan>> GetCollectionAsync();
        Task RenewBook(int id);
        Task<Loan> CreateLoan(User account, Book book);
    }
}
