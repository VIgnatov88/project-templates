﻿using Library.Models;

namespace Library.Service.Contracts
{
    public interface INotificationServices : IEntityServices<Notification>
    {
    }
}
