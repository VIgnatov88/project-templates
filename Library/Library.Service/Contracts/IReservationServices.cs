﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.Service.Contracts
{
    public interface IReservationServices : IEntityServices<Reservation>
    {
        Task<User> GetReserverAsync(Book book);
        Task<IReadOnlyCollection<Reservation>> GetCollectionAsync();
        Task<Reservation> CreateReservation(User account, Book book);
    }
}
