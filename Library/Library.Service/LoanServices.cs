﻿using Library.Database1;
using Library.Models;
using Library.Service.Contracts;
using Library.Service.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Service
{
    public class LoansServices : ILoanServices
    {
        private readonly LibraryDbContext context;

        public LoansServices(LibraryDbContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(Loan loan)
        {
            loan.EnsureNotNull();
            await context.Loans.AddAsync(loan);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Loan loan)
        {
            loan.EnsureNotNull();
            context.Loans.Remove(loan);
            await context.SaveChangesAsync();
        }

        public async Task<Loan> GetAsync(int id)
        {
            var loan = await context.Loans.FirstOrDefaultAsync(l => l.ID == id);
            loan.EnsureNotNull();
            return loan;
        }
        public async Task<User> GetAccountRenterAsync(Book book)
        {
            var reservation = await context.Loans.FirstOrDefaultAsync(r => r.Book == book);
            if (reservation == null)
                return null;
            return reservation.User;
        }

        public async Task<IReadOnlyCollection<Loan>> GetCollectionAsync()
        {
            return await context.Loans.ToListAsync();
        }
        
        public async Task<Loan> CreateLoan(User account, Book book)
        {
            var loan = new Loan
            {
                User = account,
                UserID = account.Id,
                Book = book,
                BookID = book.ID,
                BookTakenAt = DateTime.Now,
                DateToBeReturned = DateTime.Now + TimeSpan.FromDays(30)
            };
            loan.EnsureNotNull();
            await this.context.Loans.AddAsync(loan);
            return loan;
        }
        public async Task RenewBook(int id)
        {
            var loan = await GetAsync(id);
            loan.EnsureNotNull();
            loan.DateToBeReturned += TimeSpan.FromDays(30);
        }
    }
}
